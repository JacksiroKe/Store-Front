#ifndef USERWIZARD_H
#define USERWIZARD_H

#include <QMainWindow>

namespace Ui {
class UserWizard;
}

class UserWizard : public QMainWindow
{
    Q_OBJECT

public:
    explicit UserWizard(QWidget *parent = nullptr);
    ~UserWizard();

private slots:
    void on_BtnAdminProceed_clicked();

    void on_BtnGuestProceed_clicked();

    void on_BtnLogin_clicked();

private:
    Ui::UserWizard *ui;
};

#endif // USERWIZARD_H
