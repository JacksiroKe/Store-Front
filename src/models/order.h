/***************************************************************
 * Name:      Order.cpp
 * Purpose:   Defines the Order class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#ifndef ORDER_H
#define ORDER_H

#include <QMetaType>

struct Order {
    int id;
    int customerid;
    int productid;
    int quantity;
    int amount;
    QString created;
    QString updated;
};

Q_DECLARE_METATYPE(Order)

#endif // ORDER_H
