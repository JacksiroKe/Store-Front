/***************************************************************
 * Name:      productdelegate.cpp
 * Purpose:   Implements the ProductDelegate class
 * Author:    JacksiroKe (jacksiroke@gmail.com)
 * Created:   2021-01-27
 * Copyright: JacksiroKe (https://github.com/jacksiroke)
 * License:
 **************************************************************/

#include "productdelegate.h"
#include "../product.h"

#include <QApplication>
#include <QPainterPath>
#include <QPainter>
#include <QStyledItemDelegate>
#include <QStyle>
#include <QEvent>
#include <QDebug>

ProductDelegate::ProductDelegate(QObject* parent) : QStyledItemDelegate(parent) { }

ProductDelegate::~ProductDelegate()
{

}

void ProductDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.isValid()) {
        painter->save();
        QVariant var = index.data(Qt::UserRole + 1);
        Product item = var.value<Product>();

        // item Rectangular area
        QRectF rect;
        rect.setX(option.rect.x());
        rect.setY(option.rect.y());
        rect.setWidth(option.rect.width());
        rect.setHeight(option.rect.height());

        QPainterPath path;
        path.moveTo(rect.topRight());
        path.lineTo(rect.topLeft());
        path.quadTo(rect.topLeft(), rect.topLeft());
        path.lineTo(rect.bottomLeft());
        path.quadTo(rect.bottomLeft(), rect.bottomLeft());
        path.lineTo(rect.bottomRight());
        path.quadTo(rect.bottomRight(), rect.bottomRight());
        path.lineTo(rect.topRight());
        path.quadTo(rect.topRight(), rect.topRight());

        QRectF productText0, productText1, productText2, productText3, productText4, productText5;

        productText0 = QRect(rect.left() + 5, rect.top(), 50, 50);
        productText1 = QRect(productText0.right(), rect.top(), 200, 30);
        productText2 = QRect(productText1.right(), rect.top(), rect.width() - 550, 20);
        productText3 = QRect(productText2.right(), rect.top(), 100, 30);
        productText4 = QRect(productText3.right(), rect.top(), 100, 30);
        productText5 = QRect(productText4.right(), rect.top(), 100, 40);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 15, 0));
        painter->drawText(productText0, QString::number(item.id) + "#");

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText1, item.title);

        painter->setPen(QPen(QColor(Qt::black)));
        painter->setFont(QFont("Trebuchet MS", 10, 0));
        painter->drawText(productText2, item.description);

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText3, QString::number(item.quantity));

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 12, 0));
        painter->drawText(productText4, QString::number(item.price) + "/=");

        painter->setPen(QPen(Qt::black));
        painter->setFont(QFont("Trebuchet MS", 10, 0));
        painter->drawText(productText5, item.updated);

        painter->setPen(QPen(Qt::gray));
        painter->drawLine(0, rect.top() + 40, rect.width(), rect.top() + 40);

        painter->restore();
    }
}

QSize ProductDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
        return QSize(option.rect.width(), 50);
}
